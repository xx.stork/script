#!/bin/sh
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH

### 功能
# -- 安装 tcpx
# -- 安装 Docker
# -- 安装 探针

### 探针
status_type=1

# 检查系统
check_sys(){
	if [[ -f /etc/redhat-release ]]; then
		release="centos"
	elif cat /etc/issue | grep -q -E -i "debian"; then
		release="debian"
	elif cat /etc/issue | grep -q -E -i "ubuntu"; then
		release="ubuntu"
	elif cat /etc/issue | grep -q -E -i "centos|red hat|redhat"; then
		release="centos"
	elif cat /proc/version | grep -q -E -i "debian"; then
		release="debian"
	elif cat /proc/version | grep -q -E -i "ubuntu"; then
		release="ubuntu"
	elif cat /proc/version | grep -q -E -i "centos|red hat|redhat"; then
		release="centos"
    fi
	bit=`uname -m`
}

# 安装wget
install_wget(){
	if [[ ${release} == "centos" ]]; then
		yum -y install wget
	else
		apt-get -y install wget
	fi
}

# 安装wget
install_curl(){
	if [[ ${release} == "centos" ]]; then
		yum -y install curl
	else
		apt-get -y install curl
	fi
}


# 探针
install_status(){
	if [[ ${status_type} == 1 ]]; then
		wget -N --no-check-certificate https://gitlab.com/xx.stork/script/-/raw/master/ws.sh
		# bash ws.sh
	else
		wget -N --no-check-certificate https://gitlab.com/xx.stork/script/-/raw/master/ws.aws.sh
		# bash ./ws.aws.sh
	fi
}

install_tcpx(){
	wget -N --no-check-certificate "https://github.000060000.xyz/tcpx.sh" && chmod +x tcpx.sh
}

# docker
install_docker(){
	curl -fsSL https://get.docker.com/ | sh
	systemctl start docker
	systemctl enable docker
}

close_firewalld(){
	echo "$(cat /etc/selinux/config | sed "s#SELINUX=enforcing#SELINUX=disabled#g")" > /etc/selinux/config
	setsebool -P httpd_can_network_connect 1
	setenforce 0
	systemctl stop firewalld
	systemctl disable firewalld
	systemctl stop ufw
	systemctl disable ufw
}

# run_docker(){

# }

check_sys
install_wget
install_curl
install_tcpx
install_status
close_firewalld
install_docker

